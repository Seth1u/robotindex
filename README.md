# Voici comment installer/utiliser le programme JAVA :

## 1/ Recuperer le .jar dans le dossier dist et le connecteur mysql .jar dans lib_to_add

## 2/ Les placer sur le serveur dans un dossier (necessite JRE 11 pour être lancés)

## 3/ Depuis le dossier lancer la commande :

-----
$ java -cp mysql-connector-java.jar:RobotIndex.jar robotindex.RobotIndex 'param1:URL' 'param2:nbIterations' 'param3:portSrvWebNotification'
-----

3 Arguments au lancement du programme JAVA :

1 - URL du site à tester
2 - Nb d'itérations, nombre de liens parcourus par le programme (default 15)
3 - Numero du port pour le serveur web de notif (default 8080)

Utilisation de la notification d'avancement :

Le developeur pourra tester avec une requête GET l'etat d'avancement de l'algo :

( Exemple : boîte de dialogue javascript test toutes les 1 sec l'avancement ).

-> Requette GET : http://urlprogrammejava.dev:port  (format application/json)

NOTE: Le developpeur utilisant ce programme doit s'assurer avant de le lancer que le PORT utilisé pour le WebNotification est LIBRE. (e.g. avoir une fonction de test avant de lancer le programme JAVA)

