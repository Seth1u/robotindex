/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotindex;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author arthurdambrine
 */
public class UrlParser {
    
    private static URL Monsite;
    private URL aURL;
    private static List urlList, fullUrlList;
    private static final Pattern ptn
            = Pattern.compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

    private static final Pattern ptn2
            = Pattern.compile("<a\\s+(?:[^>]*?\\s+)?href=([\"'])(.*?)\\1");

    // construct
    public UrlParser() {
        urlList = new ArrayList();
        fullUrlList = new ArrayList();
    }

    public UrlParser(URL MonSite) {
        this();
        this.Monsite = MonSite;
    }

    // methods
    public void parseLine(String line) {

        urlList = captureValues(line);
        for (Object item : urlList) {
            if (!fullUrlList.contains(item)) {
                fullUrlList.add(item);
            }
        }

    }

    public void displayUrl() throws MalformedURLException {

        String lURL; // Tampon pour l'URL en cours de traitement

        for (int i = 0; i < urlList.size(); i++) {
            // Chaque URL est affichée indépendemment
            lURL = urlList.get(i).toString();
            //System.out.println(lUrl); // affiche l'URL complète

            aURL = new URL(lURL);

            // Préparation pour parcourir les URL internes
            if (Monsite.getHost().equals(aURL.getHost())) {
                if (aURL.getPath().endsWith("/")) {
                    System.out.println(aURL);
                }

            }

            //System.out.println(aURL.getPath());
            //System.out.println("Site = " + aURL.getHost()); // Affiche le nom de domaine example.com
        }

    }

    // Cette methode permet d'extraire le pattern et retourne au format liste
    public static List<String> captureValues(String largeText) {
        Matcher mtch = ptn.matcher(largeText);

        Matcher m = ptn2.matcher(largeText);

        List<String> ips = new ArrayList<String>();

        // Première regex sur url http(s)://...
        while (mtch.find()) {
            if (IsMatch(mtch.group())) {

                ips.add(mtch.group());

            }
        }

        while (m.find()) { // Deuxième regex (prendre en compte les href) 
            
             
            if(m.group(2).endsWith(".html")) {
                
                //System.out.println("TEST:  " + m.group(2));
                
                if (!m.group(2).startsWith("htt")){
                    // Si on a un lien comme href="docs/blabla.html"
                    if (!m.group(2).startsWith("/")) {
                            //System.out.println("TEST: " + m.group()) ;
                            // Ajouter un / à la fin est une astuce de différenciation, il faudra le retirer ensuite pour parcourir                        
                            if (Monsite.toString().charAt(4) == 's'){
                                ips.add("https://" + Monsite.getHost().toString() + Monsite.getPath().toString() + "/" + m.group(2) + "/");
                            } else {
                                ips.add("http://" + Monsite.getHost().toString() + Monsite.getPath().toString() + "/" + m.group(2) + "/");
                            }                                                    

                        }
                }
                                    
                if (m.group(2).startsWith("htt")){   // Si on a un lien href="http://monsite.fr/blabla.html"
                    if (!m.group(2).startsWith("/")) {
                             //System.out.println("MATCH http href: " + m.group(2)) ;                                                                             
                             ips.add(m.group(2));                                                                                
                        }
                }
            }
            
            else if (m.group(2).length() > 2) // securisation pour eviter de planter le m.group(2).charAt(1) == '/'
            {
                if (m.group(2).charAt(1) == '/') // si on a un lien de type : href:"//en.wikipedia.org"
                {
                    if (Monsite.toString().charAt(4) == 's') {
                        //System.out.println("https:"+m.group(2));
                        ips.add("https:" + m.group(2));
                    } else {
                        //System.out.println("http:"+m.group(2));
                        ips.add("http:" + m.group(2));
                    }
                } 
                
                else // si lien interne commenaçant par /
                {
                    if (m.group(2).startsWith("/")) {
                        //System.out.println("TEST: " + m.group()) ;
                        // Ajouter un / à la fin est une astuce de différenciation, il faudra le retirer ensuite pour parcourir                        
                        if (Monsite.toString().charAt(4) == 's'){
                            ips.add("https://" + Monsite.getHost().toString() + m.group(2) + "/");
                        } else {
                            ips.add("http://" + Monsite.getHost().toString() + m.group(2) + "/");
                        }                                                    
                        
                    }

                }
            }

        }

        return ips;
    }

    // Cette methode verifie si oui ou non la chaîne correspond à mon pattern
    private static boolean IsMatch(String s) {
        try {
            Matcher matcher = ptn.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }

    public static void displayFullUrlList() {

        for (int i = 0; i < fullUrlList.size(); i++) {
            // Chaque URL est affichée indépendemment
            System.out.println(fullUrlList.get(i));
        }

    }

    public static List<String> getFullUrlList() {

        return fullUrlList;

    }

    
}
