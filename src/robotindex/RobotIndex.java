/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotindex;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author arthurdambrine
 */
public class RobotIndex implements Runnable {

    private static int PORT = 8080;   
    
    private static List<String> urlList;
    private static Boolean isErrorMode = false;
      
    private static ArrayList<String> urlListInternesParcourir = new ArrayList<String>();
    private static ArrayList<String> urlListParcourus = new ArrayList<String>();
    
    private static ArrayList<String> urlListIndexed = new ArrayList<String>();
    private static ArrayList<Integer> compteurPointsIndexed = new ArrayList<Integer>();
    
    private static MessageListener messageListener;
    
    public void setMessageListener(MessageListener messageListener) {
        RobotIndex.messageListener = messageListener;
    }
    

    /**
     * @param args the command line arguments
     * @throws java.net.MalformedURLException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        
        // URL en entrée de notre programme        
        
        String urlDepart;
        int nbIterations;
        
        if (args.length < 2) {
            
            System.out.println("Pas assez d'argument en entrée, demande URL + Nb iterations");            
            //return;
             urlDepart = "https://art-dambrine.ovh";
             nbIterations = 20;
             if (args.length == 1 ){
                 urlDepart = args[0];
             }
             
        } else {
            urlDepart = args[0];
            nbIterations = Integer.parseInt(args[1]);
            
            if (args.length == 3 ){
                 PORT = Integer.parseInt(args[2]);
             }
            
        if (!args[0].startsWith("http")){            
                    System.out.println("N'est pas une url valide"); 
                    return;                                          
        }
        
        if (!args[0].contains("//")){            
            System.out.println("N'est pas une url valide"); 
            return;       
        }
        
        if (!args[0].contains(".")){
            System.out.println("N'est pas une url valide"); 
            return;                     
        }
        }
                       
        
        // demarrer le thread serveur
        
        RobotIndex myServer = new RobotIndex();
												
        // create dedicated thread to manage the client connection
        Thread thread = new Thread(myServer);
        thread.start();
        
        
        URL siteUrl = new URL(nettoyerURL(urlDepart));
        
        urlListParcourus.add(urlDepart);

        urlList = fullUrlList(siteUrl.toString());
        // "{\"message\":\"" + "Analyse premiere page" + "\"}"
        messageListener.newMessage("{\"message\":\"" + "Analyse premiere page" + "\"}");

        if (!isErrorMode) {

            URL formatUrl = null;

            System.out.println(" === Sites à indexer dans notre base\n\n");
            for (String url : urlList) {
                formatUrl = new URL(url);
                if (!formatUrl.getHost().equals(siteUrl.getHost())) {
                                        
                    // Indexer dans la liste urlListIndexed
                    if(!urlListIndexed.contains(formatUrl.getHost())){
                        System.out.println("Adding: " + formatUrl.getHost());
                        urlListIndexed.add(formatUrl.getHost());
                        compteurPointsIndexed.add(1);
                    } else {
                       // System.out.println("Index of: " + formatUrl.getHost());
                        int getIndex = urlListIndexed.indexOf(formatUrl.getHost());
                       // System.out.println("Is : " + getIndex );
                        compteurPointsIndexed.set(getIndex, compteurPointsIndexed.get(getIndex)+1);
                       // System.out.println("Have a score : " + compteurPointsIndexed.get(getIndex));
                    }
                    
                }
            }

            System.out.println("\n\n === Liens à parcourir \n\n");
            for (String url : urlList) {

                // Préparation pour parcourir les URL internes
                if (url.endsWith("/")) {
                    formatUrl = new URL(url.substring(0, url.length() - 1));
                } else {
                    formatUrl = new URL(url);                                        
                }

            if (formatUrl.getHost().equals(siteUrl.getHost())) {
                
                        if (siteUrl.getHost().equals(formatUrl.getHost())) {
                            if(isValidInterne(formatUrl.toString())){
                                
                                // Liens valides internes à parcourir: 
                                System.out.println(formatUrl);
                                urlListInternesParcourir.add(formatUrl.toString());
                            }
                            
                        }
                    }
                

            }
        }
                
        
        // Demarrer la boucle ici
        
        int cpt = 0;
        String lienCourant;
        
        if (!urlListInternesParcourir.isEmpty())
        while (cpt < nbIterations){
            // "{\"message\":\"" + "Iteration page numero " + cpt + "\"}"
        float progression = ((float)cpt / (float)nbIterations) * 100;    
        messageListener.newMessage("{\"message\":\"" + "Iteration page numero " + cpt + "\", \"progression\" : \"" + (int)progression + "\"}");
        
        System.out.println("\n\n\n ++++ Lien parcouru pour cette iteration :" + urlListInternesParcourir.get(0) + "\n");
        
            System.out.println("• urlListInternesParcourir :");
        for(int i = 0 ; i < urlListInternesParcourir.size() ; i ++)
            System.out.println("-> " + urlListInternesParcourir.get(i));
        
        lienCourant = urlListInternesParcourir.get(0);
        
        urlListParcourus.add(urlListInternesParcourir.get(0));
        siteUrl = new URL(urlListInternesParcourir.get(0));
        urlListInternesParcourir.remove(0);
        urlList = fullUrlList(siteUrl.toString());
        
        if (!isErrorMode) {

            URL formatUrl = null;

            System.out.println("\n === Sites à indexer dans notre base\n");
            for (String url : urlList) {
                formatUrl = new URL(url);
                if (!formatUrl.getHost().equals(siteUrl.getHost())) {
                    
                    // Indexer dans la liste urlListIndexed
                    if(!urlListIndexed.contains(formatUrl.getHost())){
                        System.out.println("Adding: " + formatUrl.getHost());
                        urlListIndexed.add(formatUrl.getHost());
                        compteurPointsIndexed.add(1);
                    } else {
                       // System.out.println("Index of: " + formatUrl.getHost());
                        int getIndex = urlListIndexed.indexOf(formatUrl.getHost());
                       // System.out.println("Is : " + getIndex );
                        compteurPointsIndexed.set(getIndex, compteurPointsIndexed.get(getIndex)+1);
                       // System.out.println("Have a score : " + compteurPointsIndexed.get(getIndex));
                    }
                    
                }
            }

           // System.out.println("\n\n === Liens à parcourir \n\n");
            for (String url : urlList) {

                // Préparation pour parcourir les URL internes
                if (url.endsWith("/")) {
                    formatUrl = new URL(url.substring(0, url.length() - 1));
                } else {
                    formatUrl = new URL(url);                                        
                }

            if (formatUrl.getHost().equals(siteUrl.getHost())) {
                
                        if (siteUrl.getHost().equals(formatUrl.getHost())) {
                            if(isValidInterne(formatUrl.toString())){
                                
                                // Liens valides internes à parcourir: 
                                //System.out.println(formatUrl);
                                if(!formatUrl.toString().equals(lienCourant)){ // NE PAS AJOUTER LIEN EN COURS
                                    if(!urlListInternesParcourir.contains(formatUrl.toString())){ // NON CONTENU DEDEANS
                                        if(!urlListParcourus.contains(formatUrl.toString())){
                                            urlListInternesParcourir.add(formatUrl.toString());
                                        }                                        
                                    }                                                                                                                                                
                                }
                                    
                            }
                            
                        }
                    }
                

            }
        }                                                
            if(urlListInternesParcourir.isEmpty()) break;
            cpt ++;
        } // fin de la boucle
                        
        
        // Printing all scores
        messageListener.newMessage("{\"message\":\"" + "SQL insert" + "\", \"progression\" : \"" + 100 + "\"}");
        
        System.out.println("\n\n == Printing all scores : ==");
        
        for(int i = 0; i < urlListIndexed.size();i++){
            System.out.println("N°"+i+" "+urlListIndexed.get(i) + " scored : " + compteurPointsIndexed.get(i));       
        }
        
        // Conexion à MYSQL et envoi des resultats

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //here sonoo is database name, root is username and password
            try (Connection con = DriverManager.getConnection(
                    "jdbc:mysql://art-dambrine.ovh:3306/*****", "*****", "*********")) {
                //here sonoo is database name, root is username and password
                Statement stmt = con.createStatement();
                
                // Crée le site et ne vérifie pas si il existe
                stmt.execute("INSERT INTO SITE VALUES ('"+ urlDepart +"', NOW()) ON DUPLICATE KEY UPDATE creation_date = NOW();");
                
                for(int i = 0; i < urlListIndexed.size();i++){
                    // INSERT INTO LINK VALUES('twitter.com',22,'https://art-dambrine.ovh');
                    //stmt.execute("INSERT INTO LINK VALUES('twit.com',24,'https://art-dambrine.ovh') ON DUPLICATE KEY UPDATE score = 1;");
                    stmt.execute("INSERT INTO LINK VALUES('"+ urlListIndexed.get(i) +"',"+compteurPointsIndexed.get(i)+",'"+ urlDepart+"')ON DUPLICATE KEY UPDATE score = "+compteurPointsIndexed.get(i)+";");
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        
        // Ordonne au serveur HTTP de se preparer à se fermer
        messageListener.newMessage("{\"message\":\"" + "finish" + "\", \"progression\" : \"" + 100 + "\"}");
                
        //Thread.sleep(1000); // pause 2 sec avant de fermer
        
        // Envoie un requête GET qui va fermer le serveur
        siteUrl = new URL(nettoyerURL("http://localhost:"+PORT));
        urlList = fullUrlList(siteUrl.toString());
        //thread.stop();        
        
    }

    public static List<String> fullUrlList(String siteUrl) throws MalformedURLException, IOException {

        URL url = new URL(siteUrl);
        UrlParser parser = new UrlParser(url);
        BufferedReader br = null;
        try {

            br = new BufferedReader(new InputStreamReader(url.openStream()));

        } catch (IOException iOException) {

            System.out.println("EROOR: " + iOException.getMessage() + "\n\n");
            isErrorMode = true;
        }
        String line;

        if (br != null) {
            try {
                while ((line = br.readLine()) != null) {
                    parser.parseLine(line);
                }
            } catch (IOException e) {
                //do something clever with the exception
                System.out.println(e);
            } finally {
                if (br != null) {
                    br.close();
                }
            }
        }

        return parser.getFullUrlList();

    }

    public static String nettoyerURL(String URLentry) {
        if (URLentry.endsWith("/")) {
            URLentry = URLentry.substring(0, URLentry.length() - 1);
        }

        System.out.println("URL de départ: " + URLentry + "\n\n");
        return URLentry;
    }
    
    public static Boolean isValidInterne(String formatUrl){
        
        if(formatUrl.endsWith(".png") == false &&
            formatUrl.endsWith(".js") == false &&
            formatUrl.endsWith(".svg") == false &&
            formatUrl.endsWith(".css") == false &&
            formatUrl.endsWith(".ico") == false &&
            formatUrl.endsWith(".jpg") == false &&           
            formatUrl.endsWith("#") == false){
            
            return true;
            
        } else {
            
            return false;
        }
        
    }

    @Override
    public void run() {
        // Lancement du thread server        
        JsonNotificationHTTPServer.startNotificationServer(PORT);
    }
    

    
}
